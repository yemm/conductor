package com.example.conductor.activies.activies

import android.content.Intent
import android.graphics.Typeface
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.conductor.R
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.activity_login.view.*
import java.lang.reflect.Type

class Login : AppCompatActivity(){
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        text_crea_una.setOnClickListener {
            regConduct()
        }
    }
    fun regConduct() {
        val intent = Intent(this, RegisterConductor::class.java)
        startActivity(intent)
    }
}