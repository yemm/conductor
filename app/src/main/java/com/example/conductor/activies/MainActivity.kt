package com.example.conductor.activies

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.conductor.R
import com.example.conductor.activies.activies.Login

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        login()
        finish()
    }
    fun login() {
        val intent = Intent(this, Login::class.java)
        startActivity(intent)
    }
}
